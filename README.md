  Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Draw Line and Fill Polygon                       | 1~5%      | Y         |


---

## How to use 
    
以下會介紹Web Canvas的基本功能


#### 設定顏色

本功能是利用HTML5內建的input color元件進行實踐
當點擊顏色塊後，就會跳出系統內建調色盤，如下圖所示

![](https://i.imgur.com/F63Iol9.png)

選好顏色就會顯示在外面

![](https://i.imgur.com/CjAozrE.png)

    本功能會影響：畫筆、直線、圓形、矩形、三角形、文字的顏色。


#### 設定筆跡粗細

本功能是利用HTML5內建的input range元件進行實踐
這個元件是一個拉桿的形狀，旁邊會標註目前的畫筆粗細
當value改變時，會觸發在Event使canvas的畫筆粗細改變

介面如下圖

![](https://i.imgur.com/9vGUjFg.png)

    筆跡粗細會影響：畫筆、直線、圓形、矩形、三角形、橡皮擦等功能。


#### 設定文字字體和大小

兩個功能都是利用HTML5內建的select元件進行實踐
本功能可以設定多種選項通使用者選擇
當元件的value onchange時，就會直接修改canvas內的font style和font size
介面如下圖：

![](https://i.imgur.com/YFZyOqc.png)

![](https://i.imgur.com/fbuWL8e.png)

結果如下圖：

![](https://i.imgur.com/Q3eTmxm.png)



### 筆刷功能

以下多項功能都是運用針對滑鼠的EventListner進行實作

當滑鼠按下時，會讓drawing狀態為true
並從input color的value更新Canvas的顏色
使其他EventListner執行作用

當滑鼠滑動且drawing為true時
會根據目前的筆刷執行對應工作

當滑鼠超出邊界或放開且drawing為true時
會讓drawing為false表示畫圖工作結束

#### 使用畫筆

本功能是用HTML5元素input button進行實作
點擊後會切換至該筆刷
介面如下圖所示(亮起的即為該按鈕)
    
![](https://i.imgur.com/fadrIGh.png)
    
當滑鼠按下後dawing開始
滑鼠滑動的軌跡會
持續用Canvas畫直線的功能描出筆跡
直到滑鼠放開

結果如下圖所示
    
![](https://i.imgur.com/GJUNQcX.png)



#### 橡皮擦

本功能是用HTML5元素input button進行實作
點擊後會切換至該筆刷
介面如下圖所示(亮起的即為該按鈕)

![](https://i.imgur.com/KW45RCf.png)

當滑鼠按下後drawing開始
滑鼠滑動的軌跡會
持續用Canvas清除方塊的功能清除筆跡
直到滑鼠放開

結果如下圖所示

![](https://i.imgur.com/uJYHRtw.png)



#### 畫圓
    
本功能是用HTML5元素input button進行實作
點擊後會切換至該筆刷
介面如下圖所示(亮起的即為該按鈕)
    
![](https://i.imgur.com/BJBW5U8.png)

當滑鼠按下後drawing開始
滑鼠滑動時會以點擊點和滑鼠做為直徑
圓心為以上兩點間
用Canvas畫弧的功能畫角度2pi的弧
也就是圓
直到滑鼠放開

為了讓滑鼠滑動時不會多畫圖形
在偵測到滑鼠滑動時
都會把上一個畫面刪掉

結果如下圖所示
    
![](https://i.imgur.com/v4q65Zn.png)



#### 畫矩形

本功能是用HTML5元素input button進行實作
點擊後會切換至該筆刷
介面如下圖所示(亮起的即為該按鈕)
    
![](https://i.imgur.com/fVdKcjc.png)

當滑鼠按下後drawing開始
滑鼠滑動時會以
點擊點和滑鼠做為斜邊畫出矩形
(其實就是用Canvas拉四次直線)
直到滑鼠放開

為了讓滑鼠滑動時不會多畫圖形
在偵測到滑鼠滑動時
都會把上一個畫面刪掉

結果如下圖所示

![](https://i.imgur.com/W1DSikr.png)
   


#### 如何畫三角形

本功能是用HTML5元素input button進行實作
點擊後會切換至該筆刷
介面如下圖所示(亮起的即為該按鈕)

![](https://i.imgur.com/68S9wA2.png)

當滑鼠按下後drawing開始
滑鼠滑動時會以滑鼠點擊點為頂點
滑鼠為其中一點的等腰三角形
(其實就是用Canvas拉三次直線)
直到滑鼠放開

為了讓滑鼠滑動時不會多畫圖形
在偵測到滑鼠滑動時
都會把上一個畫面刪掉

結果如下圖所示

![](https://i.imgur.com/3huImby.png)




#### 放置文字

本功能是用HTML5元素input button進行實作
點擊後會切換至該筆刷
介面如下圖所示(亮起的即為該按鈕)

![](https://i.imgur.com/JMaT3yj.png)

當滑鼠按下會在該點出現隱藏的文字方塊
文字方塊是Html元素input text

![](https://i.imgur.com/OwEf7od.png)

當輸入完文字按下Enter後就會
從文字方塊取得value
利用Canvas畫文字的功能在對應座標放入文字

結果如下圖所示

![](https://i.imgur.com/N1nbfPY.png)


#### 重置畫面：

本功能是用HTML5元素input button進行實作
介面如下圖所示(亮起的即為該按鈕)

![](https://i.imgur.com/XEodP01.png)

點擊後就會觸發Canvas清除畫布的功能
一切就清潔溜溜了

![](https://i.imgur.com/JQ9p10C.png)


#### 回到上一步

本功能是用HTML5元素input button進行實作
介面如下圖所示(亮起的即為該按鈕)

![](https://i.imgur.com/or9ojrc.png)

點擊下去就會回到上一個狀態

![](https://i.imgur.com/5imKpU5.png)

    為了要實現此功能
    首先在做完每一個動作後
    (具體時間為滑鼠放開時)
    會將目前畫面放進Array中儲存起來
    要Undo時就去取得Array中上個畫面


#### 重做下一步

本功能是用HTML5元素input button進行實作
介面如下圖所示(亮起的即為該按鈕)

![](https://i.imgur.com/K50O5gD.png)

點擊下去就會重做下一個狀態

![](https://i.imgur.com/ayKz5FT.png)

    要Redo時就去取得Array中下個畫面


#### 下載圖片

本功能是用HTML5元素a button進行實作
介面如下圖所示(亮起的即為該按鈕)

![](https://i.imgur.com/pipICUJ.png)

點擊後就會更新href的特徵
彈出下載視窗後選擇位置

![](https://i.imgur.com/mJcy282.png)

下載的檔案如圖所示

![](https://i.imgur.com/yBnVfLq.png)


#### 上傳圖片

本功能是用HTML5元素input button進行實作

點擊後會先在html創造file元素
定義其為image後開啟FileReader

介面如下圖所示(亮起的即為該按鈕)

![](https://i.imgur.com/m8CQHid.png)

接著跳出下載視窗
FileReader根據得到的URL讀進圖片

![](https://i.imgur.com/lBCIabr.png)

接著運用Canvas畫圖的功能
將圖片縮放成Canvas大小並從
FileReader得到的data進行繪圖

![](https://i.imgur.com/OCXgD1r.png)


## Function description

這裡會介紹額外多做的功能：


#### 畫直線
    
本功能是用HTML5元件input button進行實作
點擊後會切換至該筆刷
介面如下圖所示(亮起的即為該按鈕)
    
![](https://i.imgur.com/z4qf9XT.png)

當滑鼠按下後drawing開始
就會用Canvas畫直線的功能
畫出以點擊點和滑鼠為兩邊的直線
直到滑鼠放開

為了讓滑鼠滑動時不會多畫圖形
在偵測到滑鼠滑動時
都會把上一個畫面刪掉

結果如下圖所示
    
![](https://i.imgur.com/znxVLmI.png)


#### 使多邊形填滿

本功能是用HTML5元件a button進行實作
介面如下圖所示(亮起的即為該按鈕)

![](https://i.imgur.com/NZ5rE4U.png)

此button有兩種型態：Hollow和Fill
點擊後會切換到另一個狀態

![](https://i.imgur.com/xNzEjPu.png)

當button為Fill時
畫圓形、矩形、三角形
會多做Canvas的Fill功能
使圖形中間填滿

![](https://i.imgur.com/MR7ZSrC.png)


## Gitlab page link

https://106060007.gitlab.io/AS_01_WebCanvas