var c;
var ctx;
var mode = "Pen";
var drawing = false;
var lastX;
var lastY;
var size = 5;
var fstyle = "標楷體";
var fsize = 10;
var textBox;
var imageArray = new Array();;
var step = -1;
var fill = false;

window.onload = function() {
    c = document.getElementById('MyCanvas');  
    c.addEventListener('mousedown', doingMouseDown, false);
    c.addEventListener('mousemove', draw, false);	
    c.addEventListener('mouseup', doingMouseUp, false);
    c.addEventListener('mouseout', doingMouseUp, false);
    c.style.cursor = 'url(image/pen.png) 0 30, auto';
    document.getElementById("range").addEventListener("input", changeSize, false);
    console.log("add Mouse Event Listener");
    ctx = c.getContext("2d");
    ctx.strokeStyle = "black";
    ctx.fillStyle = "black";
    ctx.textAlign = "center";
    ctx.lineWidth = 5;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    textBox = document.getElementById("textBox");
    textBox.addEventListener("keydown", inputDone, false);
    textBox.style.display = "none";
    textBox.style.position = "absolute";
    ctx.font = fsize+"px "+fstyle;
    pushImage();
    console.log("initialize Done");
}

function pushImage() {
    step++;
    if(step < imageArray.length) imageArray.length = step;
    imageArray.push(ctx.getImageData(0,0,640,360));
    console.log("Now Image: "+step);
}

function changeMode(next) {
    mode = next;
    console.log(mode+" Mode");
    if(mode=="Pen") c.style.cursor = 'url(image/pen.png) 0 30, auto';
    else if(mode=="Eraser") c.style.cursor = 'url(image/era.png) 0 30, auto';
    else if(mode=="Line") c.style.cursor = 'url(image/line.png) 0 30, auto';
    else if(mode=="Circle") c.style.cursor = 'url(image/cir.png) 0 30, auto';
    else if(mode=="Rectangle") c.style.cursor = 'url(image/rect.png) 0 30, auto';
    else if(mode=="Triangle") c.style.cursor = 'url(image/tri.png) 0 30, auto';
    else if(mode=="Text") c.style.cursor = 'url(image/T.png) 0 30, auto';
    console.log(c.style.cursor);
}

function changeFill(state) {
    if(state.innerHTML=="Hollow"){
        state.innerHTML = "Fill";
        fill = true;
    }else{
        state.innerHTML = "Hollow";
        fill = false;
    }console.log("Fill: "+fill);
}

function doingMouseDown(e){
    if(!drawing){
        var color = document.getElementById("favcolor").value;
        ctx.strokeStyle = color;
        ctx.fillStyle = color;
        if(mode != "Text") drawing = true;
        [lastX, lastY] = [e.offsetX, e.offsetY];
        if(mode=="Text") inputText();
    }
}

function doingMouseUp(e){
    if(drawing){
        drawing = false;
        pushImage();
    }
}

function draw(e) {
    if (!drawing) return;
    if(mode=="Pen") drawByPen(e);
    else if(mode=="Eraser") erasing(e);
    else if(mode=="Line") drawLine(e);
    else if(mode=="Circle") drawCircle(e);
    else if(mode=="Rectangle") drawRectangle(e);
    else if(mode=="Triangle") drawTriangle(e);
}

function changeSize(){
    size = document.getElementById('range').value;
    ctx.lineWidth = size;
    document.getElementById('BrushSize').innerHTML = size;
}

function changeFontStyle(){
    var f = document.getElementById("FontStyle");
    fstyle = f.options[f.selectedIndex].innerHTML;
    ctx.font = fsize+"px "+fstyle;
    console.log("Change Font Style to "+ctx.font);
}

function changeFontSize(){
    var f = document.getElementById("FontSize");
    fsize = f.options[f.selectedIndex].innerHTML;
    ctx.font = fsize+"px "+fstyle;
    console.log("Change Font Size to "+ctx.font);
}

function drawByPen(e){
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();
    [lastX, lastY] = [e.offsetX, e.offsetY]
}

function drawLine(e){
    ctx.putImageData(imageArray[step], 0, 0);
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.stroke();
}


function drawCircle(e){
    ctx.putImageData(imageArray[step], 0, 0);
    ctx.beginPath();
    var radius = Math.hypot(Math.abs((e.offsetX-lastX)/2),Math.abs((e.offsetY-lastY)/2));
    ctx.arc((lastX+e.offsetX)/2, (lastY+e.offsetY)/2, radius, 0, 2*Math.PI);
    if(fill) ctx.fill();
    ctx.stroke();
}

function drawRectangle(e){
    ctx.putImageData(imageArray[step], 0, 0);
    ctx.beginPath();
    if(fill) ctx.fillRect(lastX, lastY, e.offsetX-lastX, e.offsetY-lastY);
    else ctx.rect(lastX, lastY, e.offsetX-lastX, e.offsetY-lastY);
    ctx.stroke();
}

function drawTriangle(e){
    ctx.putImageData(imageArray[step], 0, 0);
    ctx.beginPath();
    ctx.moveTo(lastX, lastY);
    ctx.lineTo(e.offsetX, e.offsetY);
    ctx.lineTo(2*lastX-e.offsetX, e.offsetY);
    ctx.lineTo(lastX, lastY);
    if(fill) ctx.fill();
    ctx.stroke();
}

function inputText() {
    textBox.style.display = "block";
    textBox.value = null;
    textBox.style.left = lastX+50+"px";
    textBox.style.top = lastY-25+"px";
}

function inputDone(e){
    if(e.keyCode==13){
        ctx.fillText(textBox.value, lastX-15, lastY);
        textBox.value = null;
        textBox.style.display = "none";
        pushImage();
        console.log("Input Done");
    }
}

function erasing(e){
    ctx.clearRect(lastX, lastY, size, size);
    [lastX, lastY] = [e.offsetX, e.offsetY];
}

function clearCanvas() {
    ctx.clearRect(0, 0, c.width, c.height);
    pushImage();
    console.log("Clear Canvas");
}

function Undo() {
    if(step > 0){
        step--; 
        ctx.putImageData(imageArray[step], 0, 0);
        console.log("Now Image: "+step);
    }
}

function Redo() {
    if(step < imageArray.length-1){
        step++; 
        ctx.putImageData(imageArray[step], 0, 0);
        console.log("Now Image: "+step);
    }
}

function Upload(id) {
    var upload = document.createElement('input')
    upload.addEventListener('change', readImage, false);
    upload.type = 'file';
    upload.accept = 'image/*';
    upload.id = id;
    upload.click();
}

function readImage() {
    var file = this.files[0];
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function(e){
        drawImage(this.result);
    }
}

function drawImage(data) {
    var image = new Image;
    image.src = data;
    image.onload = function(){
        ctx.drawImage(image, 0, 0, 640, 360);
        strDataURI = c.toDataURL();
        pushImage();
        console.log("Draw Image");
    }
}
function downloadImage(object) {
    object.href = c.toDataURL();
    console.log("Download Image");
}
